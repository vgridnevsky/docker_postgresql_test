Test for Docker and PostgreSQL.

I used it to debug DB connection and to find out about appropriate host
for connection.

More: [Docker documentation](https://docs.docker.com/docker-cloud/apps/service-links/) on service discovery.

> When a service is scaled up, a new hostname is created
> and automatically resolves to the new IP of the container,
> and the parent service hostname record also updates
> to include the new container’s IP

> Using service and container names as hostnames
> You can use hostnames to connect any container in your Docker Cloud account
> to any other container on your account without having to create service links or manage environment variables.
> This is the *recommended service discovery method*.

> Hostnames will always resolve to the correct IP
> for the service or container,
> and will update as the service scales up, scales down, or redeploys.
