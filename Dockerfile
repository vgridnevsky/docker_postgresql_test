FROM ubuntu:16.04

MAINTAINER Victor Gridnevsky

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y supervisor python3-pip

RUN pip3 install -U pip
RUN pip3 install -r requirements.txt

RUN mkdir -p /opt/python_projects/tests/
COPY ./pg_test.py /opt/python_projects/tests/pg_test.py

# Using:
# https://hub.docker.com/_/nginx/
# https://gist.github.com/crosbymichael/5794209
# http://michal.karzynski.pl/blog/2015/04/19/packaging-django-applications-as-docker-container-images/

# possible to install openssh-server;
# ADD sshd.conf /etc/supervisor/conf.d/sshd.conf
