#!/usr/bin/env python3

"""
Tests for a PostgreSQL availability
by trying to perform a simple request.
"""

import psycopg2

def main():
    """
    This function tries to connect to PostgreSQL and make a simple query.
    """
    conn = None
    print("Testing PostgreSQL connection.")
    try:
        conn = psycopg2.connect(
            "host=database "
            "port=5432 "
            "dbname=postgres "
            "user=Wis "
            "password=Lipton"
        )
    except psycopg2.OperationalError as op_error_exception:
        # Intercept "connection refused" exception
        print(op_error_exception)
    if conn is not None:
        cur = conn.cursor()
        cur.execute("SELECT 1;")
        print("Result:", cur.fetchone())
        cur.close()
        conn.close()

if __name__ == '__main__':
    main()
